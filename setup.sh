#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p ~/.config
mkdir -p ~/.vim/bundle/
mkdir -p ~/.fonts

git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
# Vim (~/.vim/autoload)
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Neovim (~/.local/share/nvim/site/autoload)
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim


printf "Symlinking vimrc to ~...\n"
ln -s "$DIR"/.vimrc ~/.vimrc
printf "Setting up symlinks for neovim...\n"
ln -s ~/.vim ~/.config/nvim
ln -s "$DIR"/.vimrc ~/.config/nvim/init.vim

printf "Symlinking inputrc to ~ (sets vi bindings for shells that use gnu readline)\n"
ln -s "$DIR"/.inputrc ~/.inputrc

PROFILE=~/.bashrc
OS=$(uname)


if [ "$OS" == Darwin ]; then
    #SETUP HOMEBREW
    printf 'Installing Hombrew.....\n'
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew install git wget python3 ack bash
    brew tap neovim/neovim
    brew cask install macvim java iterm2 firefox google-chrome opera steam slack 
    PROFILE=~/.bash_profile
    echo 'export PATH=/usr/local/bin:$PATH' >> "$PROFILE"
    echo 'source ~/.bash_profile' >> ~/.bashrc
    printf "Downloading powerline fonts...\n"
    printf "Don't forget to install powerline fonts!\n"
    git clone https://github.com/powerline/fonts.git
else
    DISTRO=$(grep '^ID' /etc/os-release | awk -F "=" '{ print $2 }')
    if [ "$DISTRO" == fedora ]; then 
        sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
        sudo dnf update
        printf "Installing some useful...and not so useful stuff...\n"
        sudo dnf install code neovim @kde-desktop kernel-devel yakuake liberation-fonts cscope python3-pip
        printf "Installing Chrome ...\n"
        wget -P ~/Downloads/ https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
        sudo rpm -i ~/Downloads/google-chrome*.rpm
    elif [ "$DISTRO" == ubuntu ]; then
        # Update list of available packages
        sudo apt update
        printf "Installing some useful...and not so useful stuff...\n"
        sudo apt install build-essential sakura awesome vlc python3 compton python-dev python-pip python3-dev python3-pip libxss1 libappindicator1 libindicator7 ssh ddd git i3 rofi flatpak gnome-software-plugin-flatpak latte-dock neovim cscope
        printf "Installing Chrome ...\n"
        wget -P ~/Downloads/ https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
        sudo dpkg -i ~/Downloads/google-chrome*.deb
    else
        exit -1
    fi

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    printf "Installing Spotify...\n"
    flatpak install flathub com.spotify.Client

    # Setup i3 config
    ln -s "$DIR"/i3 ~/.config/i3
    ln -s "$DIR"/i3status ~/.config/i3status
    ln -s "$DIR"/polybar ~/.config/polybar

    printf "Installing Powerline fonts...\n"
    git clone https://github.com/powerline/fonts.git ~/.fonts/powerline-fonts
    sudo fc-cache -fv
fi
    echo 'BASE16_SHELL=$HOME/.config/base16-shell/
    [ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"' >> "$PROFILE"
    echo 'export TERM=xterm-256color' >> "$PROFILE"
    echo 'export XDG_CONFIG_HOME=~/.config' >> "$PROFILE"
    printf "Adding alias for ls...\n"
    echo 'alias ls='\''exa'\''' >> "$PROFILE"
    printf "Adding alias for ls -l...\n"
    echo 'alias ll='\''exa -l'\''' >> "$PROFILE"
    printf "Adding alias for ls -a...\n"
    echo 'alias la='\''exa -a'\''' >> "$PROFILE"
    printf "Adding alias for ls -la...\n"
    echo 'alias lla='\''exa -la'\''' >> "$PROFILE"
    printf "Adding alias for cd .. ...\n"
    echo 'alias ..='\''cd ../'\''' >> "$PROFILE"
    printf "Adding alias for cd ../../ ...\n"
    echo 'alias ...='\''cd ../../'\''' >> "$PROFILE"
    printf "Adding alias for cd ../../../ ...\n"
    echo 'alias .3='\''cd ../../../'\''' >> "$PROFILE"

    pip3 install neovim

    # Set up powerline
    pip3 install powerline-shell

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

    echo '

    function build_cscope_db_func() {
        local kernelLocation="$HOME/Source/linux/include"
        local PROJDIR=$PWD
        cd $kernelLocation
        fd -a ".*\.c|.*\.h" > $PROJDIR/cscope.files
        cd $PROJDIR
        fd -a ".*\.c|.*\.h" >> $PROJDIR/cscope.files
        cscope -Rbkq
    }

    alias csbuild=\\''build_cscope_db_func\\''

    function _update_ps1() {
        PS1=$(powerline-shell $?)
    }

    if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
        PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
    fi

    export PATH="~/.local/bin:$PATH"

    export EDITOR="nvim"' >> "$PROFILE"
cp ~/.config/powerline-shell/config.py.dist ~/.config/powerline-shell/config.py

# INSTALL RUST
read -p "Install rust-lang? (y/N)?" choice
case "$choice" in
  y|Y ) printf "Installing Rust....\n"
      curl https://sh.rustup.rs -sSf | sh
      source "$PROFILE"
      cargo install exa
      cargo install bat
      cargo install fd-find
      ;;
  n|N|* ) printf "Not installing Rust...\n";;
esac
