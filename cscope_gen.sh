#!/bin/sh
fd -a -e '.py' \
-e '.java' \
-e '.c' \
-e '.h' \
-e '.rc' \
-e '*.[CH]' \
-e '.cpp' \
-e '.cc' \
-e '.hpp'  \
> cscope.files

cscope -b -q
